<!DOCTYPE html>
<?php
include_once "utilities.php";
$conn = createConnection();


if (isset($_POST["createNewUserButton"]))
{
    $email=$_POST["EmailTextBox"];
    $password=$_POST["PaswordTextBox"];
    $confpass=$_POST["ConfPaswordTextBox"];
    $name=$_POST["NameTextBox"];
    $lastName=$_POST["lastNameTextBox"];

    $valido=validarDatos($confpass, $password,$name,$lastName);

    if($valido)
    {
      $sql = "INSERT INTO users (email, password, Name, LastName) VALUES (?, ?, ?, ?)";

      $password = password_hash($confpass, PASSWORD_DEFAULT);
      $stmt = mysqli_stmt_init($conn);
      if (mysqli_stmt_prepare($stmt, $sql))
      {
        mysqli_stmt_bind_param($stmt,"ssss", $email,$password,$name,$lastName);

        mysqli_stmt_execute($stmt);


        $success = true;
      }
      header("Location: index.php");
    }
}


$selectSql = "SELECT idUsuario,Name, LastName, email FROM users";
$result = mysqli_query($conn, $selectSql);
?>
<html lang="es-BO">
  <head>
    <title>El ahorcado - Usuarios</title>
    <meta charset="UTF-8">
    <link rel="stylesheet" type="text/css" href="css/users.css">
    <script src="https://ajax.googleapis.com/ajax/libs/angularjs/1.4.10/angular.min.js"></script>
  </head>
  <body>

  <h1>New User</h1>
    <form method="POST">
      Email:
      <input type="email" name="EmailTextBox"/>
      
      </br>
      Password:
      <input type="password" name="PaswordTextBox"/>
      </br>
      Confirm your password:
      <input type="password" name="ConfPaswordTextBox" />
      </br>
      Name:
      <input type="text" name="NameTextBox" maxlength="50" />
      </br>
      Last Name:
      <input type="text" name="lastNameTextBox" maxlength="50" />
    </br>
      <input type="submit" name="createNewUserButton" value="Crear" />
    </form>

    <script>
      function validarEmail( email ) {
        expr = /^([a-zA-Z0-9_\.\-])+\@(([a-zA-Z0-9\-])+\.)+([a-zA-Z0-9]{2,4})+$/;
        if ( !expr.test(email) )
          alert("Error: La dirección de correo " + email + " es incorrecta.");
      }
    </script>
    <h1>Users</h1>
      <ul>
      <?php
        if (mysqli_num_rows($result) > 0)
        {
         ?>
        <h2>Name, email</h3>
          <?php
        while( $row = mysqli_fetch_assoc($result) )
        { ?>
          <li><a href="Login.php?id=<?= $row["idUsuario"] ?>"><?= $row["Name"] ?>, <?= $row["email"] ?></a></li>
        <?php
        }
     }
    ?>
    <ul>
    </li>
  </ul>
  </body>
</html>
